# README #


This is a university project made in Java with Play Framework, an open source web application framework.
The main purpose was to learn and use the websockets, embedded in a web application that provide a pictures sharing board.

### What the final website provides to the user###

* registration / login / logout;
* loads a personal profile photo;
* creates album and load pictures;
* share pictures with other users.

### Which topics you can find here ###

* Java;
* Web application;
* Relational database creation and management (db directory);
* SQL and use of JDBC;
* Websockets.