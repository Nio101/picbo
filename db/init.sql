drop table Auth;
drop table Photos;
drop table Albums cascade;
drop table Utente cascade;


create table Utente(
	email varchar(50) PRIMARY KEY,
	name varchar(20) NOT NULL,
	surname varchar(20) NOT NULL,
	birthdate date NOT NULL,
	nickname varchar(50) NOT NULL UNIQUE
);

create table Auth(
	email varchar(50) REFERENCES Utente(email) ON UPDATE CASCADE ON DELETE CASCADE,
	signed_email_password varchar(100) NOT NULL,
	password varchar(100) NOT NULL,
	PRIMARY KEY(email)
);

create table Albums(
	email varchar(50) UNIQUE REFERENCES Utente(email) ON UPDATE CASCADE ON DELETE CASCADE,
	album_name varchar(50) NOT NULL,
	PRIMARY KEY(email, album_name)
);

create table Photos(
	email varchar(50) REFERENCES Albums(email) ON UPDATE CASCADE ON DELETE CASCADE,
	filename varchar(100) NOT NULL,
	name varchar(50)  NOT NULL,
	format varchar(5)  NOT NULL,
	album varchar(50)  NOT NULL,
	title varchar(100) NOT NULL,
	PRIMARY KEY(email, filename)
);