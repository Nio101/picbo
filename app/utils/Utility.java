package utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.*;

import play.mvc.*;
import dbms.DBMS;

public class Utility{
		
	/**
	 * Questo metodo ritorna l'ultima sottostringa del filename
	 * delimitata dal carattere ".".
	 * Questo si suppone sia il formato dell'immagine
	 * @param 	filename: il nome del file
	 * @return	il formato dell'immagine
	 */
	public static String getFormatImage(String filename){
		String[] splittedString = filename.split("\\.");
		
		return splittedString[splittedString.length-1];
	}	
	
	
	/**
	 * Questo metodo esegue una signatura hash di una stringa utilizzando
	 * l'algoritmo SHA-256
	 * @param 	s: stringa di cui eseguire l'hash
	 * @return	digest della stringa
	 */
	public static String sign(String s){
		try{
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(s.getBytes("UTF-8"));
			byte[] digest = md.digest();
			StringBuffer sb = new StringBuffer();

			for(byte b : digest)
		        sb.append(String.format("%02x", b));

			return sb.toString();
		}
		catch(NoSuchAlgorithmException nsae){
			System.out.println("errore algoritmo hashing: " + nsae.getMessage());
			nsae.printStackTrace();
		}
		catch(UnsupportedEncodingException uee){
			System.out.println("errore codifica: " + uee.getMessage());
			uee.printStackTrace();
		}

		return null;
	}

	
	/**
	 * Questo metodo legge il valore criptato dentro il cookie dell'utente
	 * e se è valido ritorna l'email di quest ultimo
	 * @param 	cookie: il cookie
	 * @return	email dell'utente
	 */
	public static String getEmailFromCookie(Http.Cookie cookie){
		if (cookie != null){
			String signedValue = cookie.value();
			String email = DBMS.checkCookie(signedValue);
			
			return email;
		}
		
		return null;
	}
}
