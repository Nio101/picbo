package models;

import java.util.*;
import dbms.DBMS;
import play.data.validation.Constraints.*;


public class Album {

	public String name;

	@Email
	public String ownerEmail;

	@Email
	public String guestEmail;
	
	
	public ArrayList <Photo> photos;
	
	
		
	
	public Album(){	}
	
	
	public Album (String guestEmail, String ownerEmail, String album){
		this.name = album;
		this.ownerEmail = ownerEmail;
		this.guestEmail = guestEmail;
		this.photos = DBMS.getPhotos(this.ownerEmail, album);
	}

}  
