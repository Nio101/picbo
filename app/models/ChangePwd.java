package models;

import play.data.validation.Constraints.*;


public class ChangePwd {

	@Required
	public String oldPassword;

	@Required
	@MinLength(6)
	public String newPassword;

}  
