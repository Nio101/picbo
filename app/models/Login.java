package models;

import play.data.validation.Constraints.*;


public class Login {

	@Required
	@Email
	public String email;

	@Required
	public String password;

}  
