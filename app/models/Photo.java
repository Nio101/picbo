package models;


public class Photo {
	public String filename;
	
	public String name;

	public String format;

	public String album;
	
	public String title;
	
	public String ownerEmail;
	
	public String ownerNickname;
	

	public Photo(){}

	public Photo(String filename, String name, String format, String album, String title){
		this.filename = filename;
		this.name = name;
		this.format = format;
		this.album = album;
		this.title = title;
	}
	
	public Photo(String filename, String name, String format, String album, String title, String ownerEmail, String ownerNickname){
		this.ownerEmail = ownerEmail;
		this.ownerNickname = ownerNickname;
		this.filename = filename;
		this.name = name;
		this.format = format;
		this.album = album;
		this.title = title;
	}
	
}
