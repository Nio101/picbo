package models;

import java.util.*;
import dbms.DBMS;
import play.data.validation.Constraints.*;
import play.data.format.*;

public class User {

	@Required
	@Email
	public String email;

	@Required
	@MinLength(6)
	public String password;

	@Required
	public String rePassword;
	
	@Required
	public String name;

	@Required
	public String surname;
	
	@Required
	@MinLength(6)
	public String nickname;

	@Required
	@Formats.DateTime(pattern="dd/MM/yyyy")
	public Date birthDate;
	
	
	public Photo profilePic;
	
	
	public ArrayList <Photo> photos;
	
	
	public ArrayList <String> albums;
	
	
	
	public User(){	}
	
	
	public User (String name, String surname, String nickname, String email, Date date){
		this.name = name;
		this.surname = surname;
		this.nickname = nickname;
		this.email = email;
		this.birthDate = date;
		this.albums = DBMS.getAlbums(this.email);
		this.photos = DBMS.getPhotos(this.email);
		ArrayList <Photo> pic =DBMS.search("$profilepic$", email);
		if (pic != null && pic.size() > 0)
			this.profilePic = pic.get(0);
	}
	
	
	public User (String name, String surname, String nickname, String email, Date date, String album){
		this.name = name;
		this.surname = surname;
		this.nickname = nickname;
		this.email = email;
		this.birthDate = date;
		this.photos = DBMS.getPhotos(this.email, album);
		ArrayList <Photo> pic =DBMS.search("$profilepic$", email);
		if (pic != null)
			this.profilePic = pic.get(0);
	}

}  
