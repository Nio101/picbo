package controllers;

import play.mvc.*;
import play.data.*;
import views.html.*;
import java.text.Format;
import java.text.SimpleDateFormat;
import models.*;
import utils.Utility;
import play.i18n.Messages;
import dbms.DBMS;
import java.util.ArrayList;
import java.io.*;



/**
 * Authentication gestisce tutte le actions relative alla gestione degli utenti.
 * 
 * @author      Federico Matera
 */
public class Authentication extends Controller {

	static Form<Login> loginForm = form(Login.class);
	static Form<User> userForm = form(User.class);
	static Form<ChangePwd> pwdForm = form(ChangePwd.class);

	
	/**
	 * Questo metodo è invocato quando un utente vuole eseguire 
	 * il login ed invia i propri dati dal form apposito;
	 * @return	la pagina principale dell'utente loggato se le credenziali sono corrette, BADREQUEST con reindirizzamento all'index altrimenti
	 */
	public static Result login(){
		//viene creato un form ricavandolo dalla request inviata dal browser
		Form<Login> form = loginForm.bindFromRequest();
		Login data = null;
		
		//se il form ha errori (per esempio il campo email non è fomrato
		//da un indirizzo email corretto), allora viene ritornata una bad request
		//della stessa pagina dove però nel form saranno segnati i campi che hanno generato un errore
		if(form.hasErrors()) {
			return badRequest(index.render(form));
		}

		//altrimenti la variabile di tipo Login riceve i dati dal form
		data = form.get();

		//se l'autenticazione nel database è errata
		if ( !DBMS.authenticate(data.email, Utility.sign(data.password)) ){
			//viene ritornato un bad request evidenziando che il campo "passowrd" è errato
			form.reject("password", Messages.get("error.login"));
			return badRequest(index.render(form));
		}

		//altrimenti viene generato un cookie nel browser dell'utente contenente
		//l'email e la password dell'utente incatenate e criptate con un algoritmo hash
		response().setCookie("signedValue", Utility.sign(data.email+data.password), 1296000);  //1296000s = 15 giorni
		System.out.println("entra: "+data.email);
				
		return redirect(controllers.routes.Application.index());
	}


	/**
	 * Questo metodo è invocato quando un utente vuole registrarsi sul sito
	 * ed invia i propri dati dal form apposito;
	 * il metodo si preoccupa di inserire i dati dell'utente nel database.
	 * @return	la pagina principale dell'utente loggato se le credenziali sono corrette, BADREQUEST con il relativo errore altrimenti
	 */
	public static Result signup(){
		Form<User> form = userForm.bindFromRequest();

		if (form.hasErrors())
			return badRequest(signup.render(form));
		
		User data = form.get();
		
		//se il nickname è già utilizzato viene ritornato bad request
		if (DBMS.getEmailByNickname(data.nickname) != null){
			form.reject("nickname", Messages.get("error.nicknameAlreadyRegistered"));
			return badRequest(signup.render(form));
		}
		
		//se l'email è già utilizzata viene ritornato bad request
		if ( DBMS.getUser(data.email) != null){
			form.reject("email", Messages.get("error.emailAlreadyRegistered"));
			return badRequest(signup.render(form));
		}
		
		//se la password e il campo "ripeti password" non corrispondono viene ritornato bad request
		if (data.password.compareTo(data.rePassword) != 0){
			form.reject("rePassword", Messages.get("error.notSamePwd"));
			return badRequest(signup.render(form));
		}
		
		//altrimenti viene formattata la data di nascita in modo che sia compatibile
		//con le date SQL e viene registrato l'utente nel database
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");	
		DBMS.signup(data.name, data.surname, data.email, formatter.format(data.birthDate), Utility.sign(data.password), data.nickname, Utility.sign(data.email+data.password));
		System.out.println("registrato: "+data.email);
		response().setCookie("signedValue", Utility.sign(data.email+data.password), 1296000); 
		
		return redirect(controllers.routes.Application.index());
	}


	/**
	 * Questo metodo viene invocato quando un utente vuole eliminare il proprio account.
	 * Il metodo si preoccupa di eliminare tutte le fotografie dell'utente e tutte le tuple del database.
	 * Metodo per ajax.
	 * @return	OK signal se ha successo, BADREQUEST altrimenti
	 */
	public static Result deleteAccount(){
		String email = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (email != null){
			ArrayList <Photo> photos = DBMS.delUser(email);
			File photo, thumb;
			
			for (int k=0; k<photos.size(); k++){
				photo = new File("photos/"+email+"/"+photos.get(k).filename);
				thumb = new File("photos/"+email+"/thumb_"+photos.get(k).filename);
				photo.delete();
				thumb.delete();
			}
			
			photo = new File("photos/"+email);
			photo.delete();
			response().discardCookies("signedValue");
			System.out.println("disiscritto: "+email);
			
			return ok();
		}
		
		return badRequest();
	}
	
	
	/**
	 * Questo metodo è invocato quando un utente vuole eseguire il logout.
	 * Il metodo si preoccupa di eliminare il cookie dal computer dell'utente.
	 * @return	index
	 */
	public static Result logout(){
		String email = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (email != null){
			System.out.println("esce: "+email);
			
			//viene eliminato il cookie
			response().discardCookies("signedValue");
			
			return redirect(controllers.routes.Application.index());
		}

		return badRequest();
	}


	/**
	 * Questo metodo è invocato quando un utente vuole cambiare la propria password.
	 * @return	la pagina principale dell'utente
	 */
	public static Result changePassword(){
		Form<ChangePwd> form = pwdForm.bindFromRequest();
		String email = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (email != null){
			User u = DBMS.getUser(email);
			
			if (form.hasErrors())
				return badRequest(settings.render(u, form));
			
			ChangePwd data = form.get();
			
			if ( !DBMS.authenticate(email, Utility.sign(data.oldPassword)) ){
				form.reject("oldPassword", Messages.get("error.wrongPwd"));
				return badRequest(settings.render(u, form));
			}
			
			DBMS.updatePassword(email, Utility.sign(data.newPassword), Utility.sign(email+data.newPassword));
			response().setCookie("signedValue", Utility.sign(email+data.newPassword), 1296000);
			System.out.println(email+": password aggiornata");
			
			return redirect(controllers.routes.Application.index());
		}
		
		return badRequest();
	}
}
 
