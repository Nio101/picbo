package controllers;


import play.mvc.*;
import play.libs.F.*;
import models.*;
import dbms.DBMS;
import utils.Utility;
import java.io.*;
import java.util.UUID;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import org.jdesktop.swingx.graphics.GraphicsUtilities;


/**
 * Actions gestisce tutti i metodi di azioni semplici richieste dalla pagina web, 
 * come caricare fotografie o album. Un metodo ha la particolarità di gestire connessioni websocket.
 * 
 * @author      Federico Matera
 */
public class Actions extends Controller {

	/**
	 * Questo metodo è invocato quando un utente loggato manda una fotografia via websocket.
	 * @return una connessione WebSocket
	 */
	public static WebSocket<byte[]> send() {
		final String email = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (email != null){
			return new WebSocket<byte[]>() {
				boolean bName = false;
				boolean bTitle = false;
				boolean bAlbum = false;
				String filename;
				String title;
				String album;

				//onReady è invocato quando il WebSocket è pronto
				//e crea un canale in di ingresso e un canale
				//out di uscita, entrambi dello stesso tipo
				//del WebSocket ritornato
				public void onReady(WebSocket.In<byte[]> in, final WebSocket.Out<byte[]> out) {

					//onMessage viene invocato quando il server riceve
					//un messaggio su questo WebSocket
					in.onMessage(new Callback<byte[]>() {
						public void invoke(byte[] event) {
							try{
								if (!bName){
									filename= new String (event);
									bName=true;
								}
								else if (!bTitle){
									title= new String (event);
									bTitle=true;
								}
								else if (!bAlbum){
									album= new String (event);
									bAlbum=true;
								}							
								else{
									String format = Utility.getFormatImage(filename).toUpperCase();
									
									if (format.equals("JPG") || format.equals("PNG") || format.equals("GIF") || format.equals("BMP")){
										
										//genera una stringa con un UUID casuale
										String newFilename = UUID.randomUUID().toString()+"."+format;
										String dir ="photos/"+email;
										
										System.out.println("\nImmagine arrivata:\nemail: "+email+"\nnome: "+filename+"\ntitolo: "+title+"\nalbum: "+album+"\nbytes: "+event.length+"\n");
										
										//se non è presente la directory, la crea
										new File(dir).mkdir();
										
										//crea una BufferedImage leggendo dall byte array in arrivo dal socket
										BufferedImage image = ImageIO.read(new ByteArrayInputStream(event));
										
										//attraverso la classe ImageIO scrive su disco l'immagine
										ImageIO.write(image, format, new File(dir+"/"+newFilename));
										
										int width = image.getWidth();
										int height = image.getHeight();
										
										//se l'immagine arrivata è la foto di profilo
										//cancella tutte le vecchie foto profilo salvate
										//(solitamente è solo una)
										if (title.equals("$profilepic$")){
											ArrayList<Photo> oldProfilePic = DBMS.search("$profilepic$", email);
											
											for (int k=0; k<oldProfilePic.size(); k++){
												DBMS.delPhoto(email, oldProfilePic.get(k).filename);
												File photo = new File("photos/"+email+"/"+oldProfilePic.get(k).filename);
												File thumb = new File("photos/"+email+"/thumb_"+oldProfilePic.get(k).filename);
												photo.delete();
												thumb.delete();
											}
											
										}
										
										//creazione della miniatura dell'immagine arrivata
										//e sua scrittura su disco
										if (height > 200){
											BufferedImage thumbnail = GraphicsUtilities.createThumbnail(image, width*200/height , 200);
											ImageIO.write(thumbnail, format, new File(dir+"/thumb_"+newFilename));
										}
										else{
											ImageIO.write(image, format, new File(dir+"/thumb_"+newFilename));
										}
										
										//aggiunta dell'immagine nel database
										DBMS.addPhoto(email, newFilename, filename, format, album, title);
										bName=bTitle=bAlbum=false;
										
										//invio via WebSocket del filename assegnato all'immagine
										out.write(newFilename.getBytes());
									}
									else
										System.out.println("\nRicevuto file non valido\n");
										
								}
							} 
							catch(Exception e){	
								e.printStackTrace();
							}
						}
					});

					//onClose viene invocato quando il server riceve
					//un messaggio che indica l'avvenuta chiusura del WebSocket
					in.onClose(new Callback0() {
						public void invoke() {
							System.out.println(email+" chiude il socket.");
						}
					});
				}
			};
		}
		
		return null;
	}
	

	/**
	 * Questo metodo è invocato via Ajax quando un utente vuole creare un nuovo album fotografico.
	 * @param	album: il nome dell'album
	 * @return	OK signal se ha successo, BADREQUEST altrimenti
	 */
	public static Result newAlbum(String album){
		String email = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (email != null){
			DBMS.addAlbum(email, album);
			System.out.println("\nCreato nuovo album: "+album+"\n");
			
			//i metodi chiamati attraverso ajax si aspettano una risposta
			//di tipo OK per dichiarare il successo dell'operazione
			return ok();
		}
		
		return badRequest();
	}
	
	
	/**
	 * Questo metodo è invocato via Ajax quando un utente vuole cancellare una fotografia.
	 * @param	filename: il nome della fotografia da eliminare
	 * @return	OK signal se ha successo, BADREQUEST altrimenti
	 */
	public static Result deletePhoto(String filename){
		String email = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (email != null){
			DBMS.delPhoto(email, filename);
			File photo = new File("photos/"+email+"/"+filename);
			File thumb = new File("photos/"+email+"/thumb_"+filename);
			
			if (photo.delete() && thumb.delete()){		
				System.out.println("\nimmagine cancellata:\nemail: "+email+"\nnome: "+filename+"\n");
				return ok();
			}
		}
		
		return badRequest();
	}
	
	
	/**
	 * Questo metodo è invocato via Ajax quando un utente vuole eliminare un album fotografico.
	 * @param	album: il nome dell'album
	 * @return	OK signal se ha successo, BADREQUEST altrimenti
	 */
	public static Result deleteAlbum(String album){
		String email = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (email != null){
			ArrayList <Photo> photos = DBMS.delAlbum(email, album);
			File photo, thumb;
			
			for (int k=0; k<photos.size(); k++){
				photo = new File("photos/"+email+"/"+photos.get(k).filename);
				thumb = new File("photos/"+email+"/thumb_"+photos.get(k).filename);
				photo.delete();
				thumb.delete();
			}
			
			System.out.println("\nalbum eliminato:\nemail: "+email+"\nalbum: "+album+"\n");
			
			return ok();
		}
		
		return badRequest();
	}
	
	
	/**
	 * Questo metodo è invocato quando viene richiesta una fotografia
	 * nella pagina web attraverso l'indirizzo /photos/filename.
	 * @param	filename: il nome del file della fotografia
	 * @return	la fotografia richiesta
	 */
	public static Result getPhoto(String filename){
		File fileToServe= new File("photos/"+filename);
		return ok(fileToServe);
	}

}
