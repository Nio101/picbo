package controllers;

import play.mvc.*;
import play.data.*;
import views.html.*;

import java.util.ArrayList;

import models.*;
import dbms.DBMS;
import utils.Utility;


/**
 * Application è la classe principale dell'applicazione e 
 * gestisce tutte le richieste di cambio pagina da parte dell'utente.
 * 
 * @author      Federico Matera
 */
public class Application extends Controller {
	
	static Form<Login> loginForm = form(Login.class);
	static Form<User> userForm = form(User.class);
	static Form<ChangePwd> pwdForm = form(ChangePwd.class);

	
	/**
	 * Questo metodo è invocato quando il sito web è aperto per la prima volta,
	 * è l'equivalente di index.html
	 * @return      la home page
	 */
	public static Result index() {
		//request().coockies().get("signedValue") ritorna il cookie nel browser dell'utente
		//corrispondente a signedValue
		String email = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (email != null){
			User user = DBMS.getUser(email);
			
			//ritorna al client un segnale ok con la renderizzazione
			//della pagina userBoard con input "user"
			return ok(userBoard.render(user, user));
		}

		System.out.println("entra: sconosciuto");
		return ok(index.render(loginForm));
	}
	
	
	/**
	 *Questo metodo è invocato quando si richiede la pagina per le iscrizioni.
	 * @return      la pagina delle iscrizioni
	 */
	public static Result signup(){
		Http.Cookie cookie = request().cookies().get("signedValue");

		if (cookie == null)
			return ok(signup.render(userForm));

		//il redirect utilizza il file di routes per chiamare un'altra funzione
		//in questo caso index()
		return redirect(controllers.routes.Application.index());
	}
	
	
	/**
	 * Questo metodo è invocato quando un utente richiede la pagina di un album fotografico.
	 * @param		ownerNickname: il nickname del proprietario dell'album
	 * @param		albumName: il nickname del visitatore dell'album
	 * @return		la pagina dell'album
	 */
	public static Result album(String ownerNickname, String albumName){
		String guestEmail = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (guestEmail != null){
			String ownerEmail = DBMS.getEmailByNickname(ownerNickname);
			
			if (ownerEmail != null){
				User owner = DBMS.getUser(ownerEmail);
				User guest = DBMS.getUser(guestEmail);
				Album a = new Album(guestEmail, ownerEmail, albumName);
			
				return ok(album.render(owner, guest, a));
			}
		}
		
		//badRequest ritorna un segnale di bad request stampando la stringa parametro
		return badRequest("utente o album sconosciuto");
	}
	
	
	/**
	 * Questo metodo è invocato quando un utente richiede di visitare la pagina di un altro utente.
	 * @param	ownerNickname: il nickname del proprietario della pagina
	 * @return	la pagina dell'altro utente
	 */
	public static Result pageAsGuest(String ownerNickname){
		String guestEmail = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (guestEmail != null){
			String ownerEmail = DBMS.getEmailByNickname(ownerNickname);
			
			if (ownerEmail != null){
				User owner = DBMS.getUser(ownerEmail);
				User guest = DBMS.getUser(guestEmail);
				return ok(userBoard.render(owner, guest));
			}
		}
		
		return badRequest("utente sconosciuto");
	}
	
	
	
	/**
	 * Questo metodo è invocato quando un utente usa la funzione "ricerca".
	 * @param		pattern: il pattern di ricerca
	 * @return		una pagina con tutte le immagini corrispondenti al pattern
	 */
	public static Result search(String pattern) {
		String email = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (email != null){
			User user = DBMS.getUser(email);
			ArrayList<Photo> results;
			
			if (pattern == null || pattern.compareTo("") == 0)		
				return ok(research.render(user, new ArrayList<Photo>(), "Campo di ricerca vuoto"));
		
			results = DBMS.search(pattern);	
			return ok(research.render(user, results, pattern));
		}
		
		return badRequest("utente sconosciuto");
	}
	

	/**
	 * Questo metodo è invocato quando un utente vuole cambiare le proprie impostazioni.
	 * @return	la pagina delle impostazioni
	 */
	public static Result settings() {
		String email = Utility.getEmailFromCookie(request().cookies().get("signedValue"));

		if (email != null){
			User user = DBMS.getUser(email);
			return ok(settings.render(user, pwdForm));
		}

		return badRequest("utente sconosciuto");
	}
}
