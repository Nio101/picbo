package dbms;

import play.db.*;
import java.sql.*;
import java.util.ArrayList;
import models.User;
import models.Photo;


/**
 * DBMS è la classe che gestisce tutte le interrogazioni SQL con il database di basso livello.
 * 
 * @author      Federico Matera
 */
public class DBMS{
	static String loginQ = "SELECT password FROM auth WHERE email = ?";
	static String signupQ = "INSERT INTO auth values(?, ?, ?)";
	static String getUserQ = "SELECT * FROM utente WHERE email = ?";
	static String setUserQ = "INSERT INTO utente values(?, ?, ?, ?, ?)";
	static String delUserQ = "DELETE FROM utente WHERE email=?";
	static String addPhotoQ = "INSERT INTO photos values(?, ?, ?, ?, ?, ?)";
 	static String getPhotosQ = "SELECT * FROM photos WHERE email = ?";
 	static String getPhotosByAlbumQ = "SELECT * FROM photos WHERE email = ? AND album = ?";
 	static String delPhotoQ = "DELETE FROM photos WHERE email = ? AND filename = ?";
 	static String addAlbumQ = "INSERT INTO albums values(?, ?)";
 	static String getAlbumsQ = "SELECT * FROM albums WHERE email = ?";
 	static String delAlbumQ = "DELETE FROM albums WHERE email = ? AND album_name = ?";
 	static String checkCookieQ = "SELECT email FROM auth WHERE signed_email_password = ?";
 	static String getEmailByNicknameQ = "SELECT email FROM utente WHERE nickname = ?";
 	static String searchQ = "SELECT nickname, utente.email, filename, photos.name, format, album, title FROM photos JOIN utente ON photos.email=utente.email  WHERE title like ?";
 	static String searchWithEmailQ = "SELECT nickname, utente.email, filename, photos.name, format, album, title FROM photos JOIN utente ON photos.email=utente.email  WHERE title like ? AND utente.email = ?";
 	static String updatePwdQ = "UPDATE auth SET password=?,  signed_email_password=? WHERE email=?";
 	 
	
	/**
	 * Questo metodo controlla se l'email e la password inserite corrispondono a quelle del database.
	 * @param	email: indirizzo email
	 * @param	password: password criptata precedentemente
	 * @return	true se l'email e la password inserite corrispondono a quelle del database, falso altrimenti
	 */
	public static boolean authenticate(String email, String password){
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try{
			//crea la connessione al database settato in application.conf
			connection = DB.getConnection();
			
			//prepara lo statement SQL caricando la stringa per la query
			pstmt = connection.prepareStatement(loginQ);
			pstmt.clearParameters();
			
			//imposta il primo parametro con il valore "email"
			pstmt.setString(1, email);
			
			//esegue la query
			rs = pstmt.executeQuery();

			if (rs.next()){
				//se l'interrogazione ha dato almeno un risultato
				//estrae il valore "password" dal result set
				String dbPassword= rs.getString("password");
				rs.close();
				connection.close();

				//se la password fornita dall'utente è uguale 
				//a quella nel database ritorna true
				if (dbPassword.compareTo(password) == 0)
					return true;
				else
					return false;
			}
			else
				return false;
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (authenticate): " + sqle.getMessage());
			sqle.printStackTrace();
		}
		
		return false;
	}

	
	/**
	 * Questo metodo permette di inserire una nuova iscrizione nel database.
	 * @param	name: nome dell'utente
	 * @param	surname: cognome dell'utente
	 * @param	email: indirizzo email dell'utente
	 * @param	date: data di nascita  dell'utente in stringa accettata dal DBMS
	 * @param	password: password  già criptata con algoritmo hash
	 * @param	nickname: nickname dell'utente
	 * @param	signedEmailPass: stringa criptata con algoritmo hash di email+password,
	 * 			sarà inserita nel cookie dell'utente e confrontata con questo valore
	 */
	public static void signup(String name, String surname, String email, String date,  String password, String nickname, String signedEmailPass){
		Connection connection = null;
		PreparedStatement pstmt = null;

		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(setUserQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			pstmt.setString(2, name);
			pstmt.setString(3, surname);
			pstmt.setDate(4, Date.valueOf(date));
			pstmt.setString(5, nickname);
			pstmt.executeUpdate();								//aggiungo al database i dati personali dell'utente
			pstmt = connection.prepareStatement(signupQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			pstmt.setString(2, signedEmailPass);
			pstmt.setString(3, password);
			pstmt.executeUpdate();								//aggiungo al database i dati di login dell'utente

			connection.close();
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (signup): " + sqle.getMessage());
			sqle.printStackTrace();
		}
	}
	
	
	/**
	 * Questo metodo permette di cambiare la password di un utente nel database.
	 * @param	email: indirizzo email dell'utente
	 * @param	password: password criptata precedentemente
	 * @param	signedEmailPass: stringa composta da email+password criptata precedentemente, serve per il controllo del cookie
	 */
	public static void updatePassword(String email,  String password, String signedEmailPass){
		Connection connection = null;
		PreparedStatement pstmt = null;

		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(updatePwdQ);
			pstmt.clearParameters();
			pstmt.setString(1, password);
			pstmt.setString(2, signedEmailPass);
			pstmt.setString(3, email);
			pstmt.executeUpdate();								//aggiungo al database i dati personali dell'utente
			connection.close();
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (signup): " + sqle.getMessage());
			sqle.printStackTrace();
		}
	}
	
	
	/**
	 * Questo metodo permette di recuperare dal database tutti i dati di un utente 
	 * e gli URL di tutte le sue fotografie.
	 * @param	email: indirizzo email dell'utente
	 * @return	un oggetto di tipo User con i dati dell'utente se l'email è presente nel database, null altrimenti
	 */
	public static User getUser(String email){
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String name, surname, nickname;
		Date date;

		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(getUserQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();

			if (rs.next()){
				name=rs.getString("name");
				surname=rs.getString("surname");
				nickname=rs.getString("nickname");
				date=rs.getDate("birthdate");
				
				rs.close();
				connection.close();
				
				return new User(name, surname, nickname, email, date);
			}
			else
				return null;
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (getUser): " + sqle.getMessage());
			sqle.printStackTrace();
		}
	

		return null;
	}
	
	
	/**
	 * Questo metodo permette di recuperare dal database tutti i dati di un utente 
	 * e gli URL delle sole fotografie presenti nell'album richiesto.
	 * @param	email: indirizzo email dell'utente
	 * @param	album: album dell'utente di cui sono richieste le fotografie
	 * @return	un oggetto di tipo User con i dati dell'utente se l'email è presente nel database, null altrimenti
	 */
	public static User getUser(String email, String album){
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String name, surname, nickname;
		Date date;

		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(getUserQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();

			if (rs.next()){
				name=rs.getString("name");
				surname=rs.getString("surname");
				nickname=rs.getString("nickname");
				date=rs.getDate("birthdate");

				rs.close();
				connection.close();

				return new User(name, surname, nickname, email, date, album);
			}
			else
				return null;
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (getUser): " + sqle.getMessage());
			sqle.printStackTrace();
		}

		return null;
	}

	
	/**
	 * Questo metodo permette di eliminare tutti i dati dell'utente dal database.
	 * L'interrogazione SQL elimina le credenziali solo dalla tabella Utente ma, per costruzione del database,
	 * le tuple di altre tabelle collegate all'utente verranno eliminate in cascata dal DBMS.
	 * @param	email: indirizzo email dell'utente
	 * @return	un ArrayList di oggetti Photo contentente tutte le fotografie dell'utente che dovranno essere eliminate dal disco, 
	 * 			null se ci sono errori
	 */
	public static ArrayList <Photo> delUser(String email){
		Connection connection = null;
		PreparedStatement pstmt = null;
		ArrayList<Photo> photos =  getPhotos(email);
		
		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(delUserQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			pstmt.executeUpdate();
			connection.close();
			
			return photos;
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (delUser): " + sqle.getMessage());
			sqle.printStackTrace();
		}
		
		return null;
	}
	
	
	/**
	 * Questo metodo permette di aggiungere i dati di una fotografia nel database.
	 * @param	email: indirizzo email dell'utente
	 * @param	filename: nome del file su disco
	 * @param	name: nome originale del file caricato
	 * @param	format: formato dell'immagine ("jpg"|"png2|"gif"|"bmp")
	 * @param	album: album in cui la fotografia verrà visualizzata
	 * @param	title: titolo della fotografia
	 */
	public static void addPhoto(String email, String filename, String name, String format, String album, String title){
		Connection connection = null;
		PreparedStatement pstmt = null;

		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(addPhotoQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			pstmt.setString(2, filename);
			pstmt.setString(3, name);
			pstmt.setString(4, format);
			pstmt.setString(5, album);
			pstmt.setString(6, title);
			pstmt.executeUpdate();
			connection.close();
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (addPhoto): " + sqle.getMessage());
			sqle.printStackTrace();
		}
	}
	
	
	/**
	 * Questo metodo permette di eliminare una fotografia dal database;
	 * attenzione: non elimina la foto dal disco.
	 * @param	email: indirizzo email dell'utente
	 * @param	filename: nome del file della fotografia
	 */
	public static void delPhoto(String email, String filename){
		Connection connection = null;
		PreparedStatement pstmt = null;

		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(delPhotoQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			pstmt.setString(2, filename);
			pstmt.executeUpdate();
			connection.close();
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (delPhoto): " + sqle.getMessage());
			sqle.printStackTrace();
		}
	}


	/**
	 * Questo metodo permette di recuperare i dati di tutte le fotografie dell'utente dal database;
	 * @param	email: indirizzo email dell'utente
	 * @return	un ArrayList contenente tutte le fotografie dell'utente sottoforma di classe Photo, null se ci sono errori
	 */
	public static ArrayList <Photo> getPhotos(String email){
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList <Photo> photos = new ArrayList <Photo> ();
		
		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(getPhotosQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();

			while(rs.next()){
				Photo p = new Photo(rs.getString("filename"), rs.getString("name"), rs.getString("format"), rs.getString("album"), rs.getString("title"));
				photos.add(p);
			}
			
			rs.close();
			connection.close();
			
			return photos;
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (getPhotos): " + sqle.getMessage());
			sqle.printStackTrace();
		}

		return null;
	}

	
	/**
	 * Questo metodo permette di recuperare i dati delle fotografie dell'utente 
	 * appartenenti ad un determinato album fotograficodal database;
	 * @param	email: indirizzo email dell'utente
	 * @param	album: album richiesto
	 * @return	un ArrayList contenente tutte le fotografie dell'utente sottoforma di classe Photo, null se ci sono errori
	 */
	public static ArrayList <Photo> getPhotos(String email, String album){
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList <Photo> photos = new ArrayList <Photo> ();
		
		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(getPhotosByAlbumQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			pstmt.setString(2, album);
			rs = pstmt.executeQuery();

			while(rs.next()){
				Photo p = new Photo(rs.getString("filename"), rs.getString("name"), rs.getString("format"), rs.getString("album"), rs.getString("title"));
				photos.add(p);
			}
			
			rs.close();
			connection.close();
			
			return photos;
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (getPhotos): " + sqle.getMessage());
			sqle.printStackTrace();
		}

		return null;
	}
	
	
	/**
	 * Questo metodo permette di aggiungere i dati di un nuovo album nel database.
	 * @param	email: indirizzo email dell'utente
	 * @param	album: nome dell'album
	 */
	public static void addAlbum(String email, String album){
		Connection connection = null;
		PreparedStatement pstmt = null;

		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(addAlbumQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			pstmt.setString(2, album);
			pstmt.executeUpdate();
			connection.close();
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (addAlbum): " + sqle.getMessage());
			sqle.printStackTrace();
		}
	}
	
	
	/**
	 * Questo metodo permette di recuperare il nome di tutti gli album fotografici di un utente dal database;
	 * @param	email: indirizzo email dell'utente
	 * @return	un ArrayList di String contenente i nomi di tutti gli album dell'utente, null se ci sono errori
	 */
	public static ArrayList <String> getAlbums(String email){
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList <String> albums = new ArrayList <String> ();
		
		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(getAlbumsQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();

			while(rs.next()){
				albums.add(rs.getString("album_name"));
			}
			
			rs.close();
			connection.close();
			
			return albums;
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (getAlbums): " + sqle.getMessage());
			sqle.printStackTrace();
		}

		return null;
	}


	/**
	 * Questo metodo permette di eliminare un album dal database;
	 * attenzione: elimina anche le foto corrispondenti a questo album 
	 * dal database ma non elimina la foto dal disco.
	 * @param	email: indirizzo email dell'utente
	 * @param	album: nome dell'album da eliminare
	 */
	public static ArrayList <Photo> delAlbum(String email, String album){
		Connection connection = null;
		PreparedStatement pstmt = null;

		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(delAlbumQ);
			pstmt.clearParameters();
			pstmt.setString(1, email);
			pstmt.setString(2, album);
			pstmt.executeUpdate();
			connection.close();
			
			return getPhotos(email, album);
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (delAlbum): " + sqle.getMessage());
			sqle.printStackTrace();
		}
		
		return null;
	}
	
	
	/**
	 * Questo metodo permette di controllare se il cookie dell'utente
	 * contiene un valore corretto ed eventualmente recuperare l'email corrispondente.
	 * @param	signedValue: stringa criptata all'interno del cookie
	 * @return	l'email dell'utente corrispondente
	 */
	public static String checkCookie(String signedValue){
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(checkCookieQ);
			pstmt.clearParameters();
			pstmt.setString(1, signedValue);
			rs = pstmt.executeQuery();

			if (rs.next()){
				String email= rs.getString("email");
				rs.close();
				connection.close();

				return email;
			}
			else
				return null;
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (checkCookie): " + sqle.getMessage());
			sqle.printStackTrace();
		}
		
		return null;
	}
	
	
	/**
	 * Questo metodo permette di recuperare l'email di un utente attraverso il suo nickname.
	 * @param	nickname: nickname dell'utente
	 * @return	l'email dell'utente
	 */
	public static String getEmailByNickname(String nickname){
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(getEmailByNicknameQ);
			pstmt.clearParameters();
			pstmt.setString(1, nickname);
			rs = pstmt.executeQuery();

			if (rs.next()){
				String email= rs.getString("email");
				rs.close();
				connection.close();

				return email;
			}
			else
				return null;
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (getEmailByNickname): " + sqle.getMessage());
			sqle.printStackTrace();
		}
		
		return null;
	}
	
	
	/**
	 * Questo metodo permette di ricercare una stringa tra i titoli di tutte le foto presenti nel database.
	 * @param	pattern: stringa da cercare
	 * @return	un ArrayList di Photo contenente tutte le fotografie che hanno il pattern presente nel loro titolo, null se ci sono errori
	 */
	public static ArrayList <Photo> search(String pattern){
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList <Photo> photos = new ArrayList <Photo> ();
		
		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(searchQ);
			pstmt.clearParameters();
			pstmt.setString(1, "%"+pattern+"%");
			rs = pstmt.executeQuery();

			while(rs.next()){
				Photo p = new Photo(rs.getString("filename"), rs.getString("name"), rs.getString("format"), rs.getString("album"), rs.getString("title"), rs.getString("email"), rs.getString("nickname"));
				photos.add(p);
			}
			
			rs.close();
			connection.close();
			
			return photos;
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (search): " + sqle.getMessage());
			sqle.printStackTrace();
		}
	

		return null;
	}
	
	
	/**
	 * Questo metodo permette di ricercare una stringa tra i titoli delle foto di un determinato utente.
	 * @param	pattern: stringa da cercare
	 * @param	email: email dell'utente proprietario delle foto da cercare
	 * @return	un ArrayList di Photo contenente tutte le fotografie che hanno il pattern presente nel loro titolo, null se ci sono errori
	 */
	public static ArrayList <Photo> search(String pattern, String email){
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList <Photo> photos = new ArrayList <Photo> ();
		
		try{
			connection = DB.getConnection();
			pstmt = connection.prepareStatement(searchWithEmailQ);
			pstmt.clearParameters();
			pstmt.setString(1, "%"+pattern+"%");
			pstmt.setString(2, email);
			rs = pstmt.executeQuery();

			while(rs.next()){
				Photo p = new Photo(rs.getString("filename"), rs.getString("name"), rs.getString("format"), rs.getString("album"), rs.getString("title"), rs.getString("email"), rs.getString("nickname"));
				photos.add(p);
			}
			
			rs.close();
			connection.close();
			
			return photos;
		}
		catch(SQLException sqle){
			System.out.println("errore SQL (search): " + sqle.getMessage());
			sqle.printStackTrace();
		}
	

		return null;
	}
	
}
